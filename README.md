# MELA

This repository provides tools to reweight the [NanoAOD](https://twiki.cern.ch/twiki/bin/view/CMSPublic/WorkBookNanoAOD) POWHEG samples.


## Installation recipe

Set up CMSSW:

```sh
cmsrel CMSSW_10_2_13
cd CMSSW_10_2_13/src
cmsenv 
```

Clone and build the `ZZMatrixElement` package:

```sh
git clone https://github.com/cms-analysis/HiggsAnalysis-ZZMatrixElement.git ZZMatrixElement
git checkout -b from-v223 v2.2.3
cd ZZMatrixElement
. setup.sh -j $(nproc)
cd ../
```

Clone and build the `MelaAnalytics` package:

```sh
git clone git@github.com:usarica/MelaAnalytics.git
scramv1 b -j $(nproc)
```

Clone and build this repository:

```sh
git clone ssh://git@gitlab.cern.ch:7999/mmahdavi/hzz2l2nu_mela.git
scramv1 b -j $(nproc)
```

## Examples

```sh
computer --output output.root --config config/GG_SIG_ZZ_POWHEG.yaml --sample-hmass SAMPLE_HIGGS_MASS INPUT_File_1 [INPUT_FILE_2 [... [INPUT_FILE_N]]]
```

