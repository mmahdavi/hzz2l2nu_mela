#include "LHEHandler.h"

#include <iostream>

#include "HiggsComparators.h"

namespace pdg = PDGHelpers;

LHEHandler::LHEHandler(TTreeReader &reader, MELAEvent::CandidateVVMode VVMode,
    int VVDecayMode)
    : hMass{0},
      VVMode_{VVMode},
      VVDecayMode_{VVDecayMode},
      bestCandidate_{nullptr},
      melaEvent_{nullptr},
      ptArray_{reader, "LHEPart_pt"},
      etaArray_{reader, "LHEPart_eta"},
      phiArray_{reader, "LHEPart_phi"},
      massArray_{reader, "LHEPart_mass"},
      incomingPzArray_{reader, "LHEPart_incomingpz"},
      pdgIdArray_{reader, "LHEPart_pdgId"},
      statusArray_{reader, "LHEPart_status"},
      numPart_{reader, "nLHEPart"} {}


MELACandidate* LHEHandler::GetBestCandidate() {
  mothers_.clear();
  stableParticles_.clear();
  BuildBestCandidate();
  return bestCandidate_.get();
}


void LHEHandler::BuildBestCandidate() {
  melaEvent_ = std::make_unique<MELAEvent>();
  TLorentzVector p4, lepsp4;
  for (unsigned int i = 0; i < *numPart_.Get(); i++) {
    auto const &pt = ptArray_[i], &eta = etaArray_[i], &phi = phiArray_[i];
    auto const &mass = massArray_[i], &incomingPz = incomingPzArray_[i];
    auto const &pdgId = pdgIdArray_[i];
    auto const &status = statusArray_[i];

    p4.SetPtEtaPhiM(pt, eta, phi, mass);

    if (pdg::isALepton(pdgId) or pdg::isANeutrino(pdgId))
      lepsp4 += p4;

    if (status == -1) { // Consider it as a mother particle.
      p4.SetPxPyPzE(0, 0, incomingPz, std::fabs(incomingPz));
      mothers_.push_back(std::make_unique<MELAParticle>(pdgId, p4));
      mothers_.back()->genStatus = status;
    } else if (status == 1) {
      stableParticles_.push_back(std::make_unique<MELAParticle>(pdgId, p4));
      stableParticles_.back()->genStatus = status;
    }
  }

  hMass = lepsp4.M();
  // Add mother particles.
  for (auto &mother : mothers_)
    melaEvent_->addMother(mother.get());

  for (auto &particle : stableParticles_) {
    particle->addMother(mothers_[0].get());
    particle->addMother(mothers_[1].get());
  }

  // Add the specified associated particles.
  for (auto &particle : stableParticles_) {
    if (pdg::isAKnownJet(particle->id) and not(pdg::isATopQuark(particle->id)))
      melaEvent_->addJet(particle.get());

    else if (pdg::isALepton(particle->id))
      melaEvent_->addLepton(particle.get());

    else if (pdg::isANeutrino(particle->id))
      melaEvent_->addNeutrino(particle.get());

    else if (pdg::isAPhoton(particle->id))
      melaEvent_->addPhoton(particle.get());
  }

  melaEvent_->constructVVCandidates(VVMode_, VVDecayMode_);
  melaEvent_->addVVCandidateAppendages();

  MELACandidate *cand = HiggsComparators::candidateSelector(*melaEvent_,
      HiggsComparators::BestZ1ThenZ2ScSumPt, VVMode_); 
  bestCandidate_ = std::make_unique<MELACandidate>(*cand);
}

