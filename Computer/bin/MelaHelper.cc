#include "MelaHelper.h"

#include <iostream>

#include <boost/algorithm/string/replace.hpp>
#include <boost/filesystem.hpp>

#include <yaml-cpp/yaml.h>

#include <MELACandidateRecaster.h>


namespace fs = boost::filesystem;

namespace MelaHelper {
  std::unique_ptr<Mela> melaHandle(nullptr);

  void SetupMela(float const &higgsMass, float const &sqrts,
      TVar::VerbosityLevel vl) {
    if (not melaHandle and sqrts > 0 and higgsMass >= 0.0)
      melaHandle = std::make_unique<Mela>(sqrts, higgsMass, vl);
  }


  void ClearMela() {
    melaHandle.reset();
  }


  MelaHandler::MelaHandler(TTree &tree, std::string const &sampleHMass,
      std::string const &cfgPath="")
      : isClean_{true},
        tree_{tree},
        melaCand_{nullptr},
        optionsList_{},
        hypotheses_{},
        aliasedHypos_{},
        computers_{},
        clusters_{},
        matchedClusters_{},
        branches_{} {

    if (cfgPath.empty())
      throw std::runtime_error(
          "Path to MELA configuration file was not provided.");

    if (not fs::exists(cfgPath)) {
      std::ostringstream message;
      message << "MELA configuration file '" << cfgPath << "' doesn't exist.";
      throw std::runtime_error(message.str());
    }

    YAML::Node melaConfigs = YAML::LoadFile(cfgPath);
    std::string optStr;
    for (auto const &opt : melaConfigs) {
      optStr = opt.as<std::string>();
      boost::replace_all(optStr, "<HMASS>", sampleHMass);
      optionsList_.emplace_back(optStr);
    }
}


  MelaHandler::~MelaHandler() {
    if (not isClean_)
      Clear();
  }


  void MelaHandler::PrepareComputation() {
    // Create hypotheses and computations w.r.t. the provided options. 
    for (auto &optStr : optionsList_) {
      MELAOptionParser opt(optStr);
      float defVal = opt.getDefaultME();
      std::string name = opt.getName();
      MELAHypothesis *hypo = new MELAHypothesis(melaHandle.get(), optStr);
      MELAComputation *computer = new MELAComputation(hypo);
      hypotheses_.push_back(hypo);
      computers_.push_back(computer);
      GMECHelperFunctions::addToMELACluster(computer, clusters_);

      if (opt.isAliased())
        aliasedHypos_.push_back(hypo);

      if (opt.doBranch()) {
      BranchHelpers::MELABranch *branch = new BranchHelpers::MELABranch(
          &tree_, name, defVal, computer);
      branches_.push_back(branch);
      }
    }

    for (auto & computer: computers_)
      computer->addContingencies(aliasedHypos_);

    if (not clusters_.empty()) {
      std::cout << "ME clusters:" << std::endl;
      for (auto const cluster : clusters_) {
        std::cout << "\t- Cluster " << cluster->getName() <<
          " has " << cluster->getComputations()->size() <<
          " computations registered." << std::endl;
      }
    }

    isClean_ = false;
  }


  void MelaHandler::ComputeClusters() {
    melaCand_ = melaHandle->getCurrentCandidate();
    if (not melaCand_)
      return;

    ComputeCommonClusters();
    ComputeNoInitialQClusters();
    ComputeNoInitialGClusters();
    ComputeNoAssociatedGClusters();
    ComputeBestNLOVBFApproxClusters();
    ComputeBestNLOVHApproxClusters('Z');
    ComputeBestNLOVHApproxClusters('W');
  }


  void MelaHandler::PushBranches() {
    for (auto branch : branches_)
      branch->setVal();

    for (auto cluster : clusters_)
      cluster->reset();
  }


  void MelaHandler::ComputeCommonClusters() {
    for (auto cluster : clusters_) {
      if (cluster->getName() != "Common")
        continue;
      cluster->computeAll();
      cluster->update();
    }
  }


  void MelaHandler::ComputeNoInitialQClusters() {
    matchedClusters_.clear();
    // Check if there is any "NoInitialQ" cluster.
    for (auto cluster : clusters_)
      if (cluster->getName() == "NoInitialQ")
        matchedClusters_.push_back(cluster);

    if (matchedClusters_.empty())
      return;

    // Assign 0 to the Ids of quark mothers.
    int numMothers = melaCand_->getNMothers();
    std::vector<int> mothersIds;
    for (int i = 0; i < numMothers; i++) {
      int &id = melaCand_->getMother(i)->id;
      mothersIds.push_back(id);
      if (PDGHelpers::isAQuark(id))
        id = 0;
    }

    for (auto cluster : matchedClusters_) {
      cluster->computeAll();
      cluster->update();
    }

    // Restoring the mothers Ids.
    for (int i = 0; i < numMothers; i++)
      melaCand_->getMother(i)->id = mothersIds[i];
  }


  void MelaHandler::ComputeNoInitialGClusters() {
    matchedClusters_.clear();
    // Check if there is any "NoInitialG" cluster.
    for (auto cluster : clusters_)
      if (cluster->getName() == "NoInitialG")
        matchedClusters_.push_back(cluster);

    if (matchedClusters_.empty())
      return;

    // Assign 0 to the Ids of gluon mothers.
    int numMothers = melaCand_->getNMothers();
    std::vector<int> mothersIds;
    for (int i = 0; i < numMothers; i++) {
      int &id = melaCand_->getMother(i)->id;
      mothersIds.push_back(id);
      if (PDGHelpers::isAGluon(id))
        id = 0;
    }

    for (auto cluster : matchedClusters_) {
      cluster->computeAll();
      cluster->update();
    }

    // Restoring the mothers Ids.
    for (int i = 0; i < numMothers; i++)
      melaCand_->getMother(i)->id = mothersIds[i];
  }


  void MelaHandler::ComputeNoAssociatedGClusters() {
    matchedClusters_.clear();
    // Check if there is any "NoAssociatedG" cluster.
    for (auto cluster : clusters_)
      if (cluster->getName() == "NoAssociatedG")
        matchedClusters_.push_back(cluster);

    if (matchedClusters_.empty())
      return;

    // Assign 0 to the Ids of associated gluons.
    int numAssociatedJets = melaCand_->getNAssociatedJets();
    std::vector<int> jetsIds;
    for (int i = 0; i < numAssociatedJets; i++) {
      int &jetId = melaCand_->getAssociatedJet(i)->id;
      jetsIds.push_back(jetId);
      if (PDGHelpers::isAGluon(jetId))
        jetId = 0;
    }

    for (auto cluster : matchedClusters_) {
      cluster->computeAll();
      cluster->update();
    }

    // Restoring the jets Ids.
    for (int i = 0; i < numAssociatedJets; i++)
      melaCand_->getAssociatedJet(i)->id = jetsIds[i];
  }


  void MelaHandler::ComputeBestNLOVBFApproxClusters() {
    matchedClusters_.clear();
    // Check if there is any "BestNLOVBFApproximation" cluster.
    for (auto cluster : clusters_)
      if (cluster->getName() == "BestNLOVBFApproximation")
        matchedClusters_.push_back(cluster);

    if (matchedClusters_.empty())
      return;

    MELACandidateRecaster recaster(TVar::JJVBF);
    MELACandidate modifiedCand, *modifiedCandPtr = &modifiedCand;
    recaster.copyCandidate(melaCand_, modifiedCandPtr);
    recaster.deduceLOVBFTopology(modifiedCandPtr);
    melaHandle->setCurrentCandidate(modifiedCandPtr);

    for (auto cluster : matchedClusters_) {
      cluster->computeAll();
      cluster->update();
    }
  }


  void MelaHandler::ComputeBestNLOVHApproxClusters(char const V) {
    TVar::Production candScheme;
    std::string  clusterName;
    if (V == 'Z') {
      candScheme = TVar::Had_ZH;
      clusterName = "BestNLOZHApproximation";
    }
    else if (V == 'W') {
      candScheme = TVar::Had_WH;
      clusterName = "BestNLOWHApproximation";
    }
    else
      return;

    matchedClusters_.clear();
    // Check if there is any "BestNLO{Z,W}HApproximation" cluster.
    for (auto cluster : clusters_)
      if (cluster->getName() == clusterName)
        matchedClusters_.push_back(cluster);

    if (matchedClusters_.empty())
      return;

    // Search for the best associated Vector Boson.
    MELACandidateRecaster recaster(candScheme);
    MELACandidate modifiedCand, *modifiedCandPtr = &modifiedCand;
    MELAParticle *bestAV = MELACandidateRecaster::getBestAssociatedV(
        melaCand_, candScheme);
    if (bestAV) {
      recaster.copyCandidate(melaCand_, modifiedCandPtr);
      recaster.deduceLOVHTopology(modifiedCandPtr);
      melaHandle->setCurrentCandidate(modifiedCandPtr);
    }

    else
      // No associated Vector Boson found.
      return;

    for (auto cluster : matchedClusters_) {
      cluster->computeAll();
      cluster->update();
    }
  }


  void MelaHandler::Clear() {
    if (isClean_)
      return;

    for (auto hypo : hypotheses_)
      delete hypo;

    for (auto computer : computers_)
      delete computer;

    for (auto cluster : clusters_)
      delete cluster;

    for (auto branch : branches_)
      delete branch;

    hypotheses_.clear();
    computers_.clear();
    clusters_.clear();
    branches_.clear();
    isClean_ = true;
  }

}
