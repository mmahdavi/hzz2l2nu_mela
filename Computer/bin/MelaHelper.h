#ifndef MELAHELPER_H
#define MELAHELPER_H

#include "TTree.h"

#include "Mela.h"

#include "GMECHelperFunctions.h"
#include "MELAEvent.h"


namespace MelaHelper {
  extern std::unique_ptr<Mela> melaHandle;

  // Setup Mela opject.
  void SetupMela(float const &higgsMass, float const &sqrts,
      TVar::VerbosityLevel vl);

  // Reset the Mela shared pointer to nullptr.
  void ClearMela();

  class MelaHandler {
    public:
      // Constructor
      MelaHandler(TTree &tree, std::string const &sampleHMass,
          std::string const &cfgPath);

      // Destructor
      ~MelaHandler();

      // Set the hypotheses, computers, branches and clusters.  
      void PrepareComputation();

      // Compute the ME for all clusters.
      void ComputeClusters();
      
      // Save the computations in MELABranches.
      void PushBranches();

      // Clear all hypotheses, computers, branches and clusters.
      void Clear();

    private:
      bool isClean_;

      TTree &tree_;

      MELACandidate *melaCand_;

      std::vector<std::string> optionsList_; 

      std::vector<MELAHypothesis*> hypotheses_, aliasedHypos_;

      std::vector<MELAComputation*> computers_;

      std::vector<MELACluster*> clusters_, matchedClusters_;

      std::vector<BranchHelpers::MELABranch*> branches_;

      // Compute Common Mela Clusters.
      void ComputeCommonClusters();

      // Compute NoInitialQ Mela Clusters.
      void ComputeNoInitialQClusters();

      // Compute NoInitialG Mela Clusters.
      void ComputeNoInitialGClusters();

      // Compute NoAssociatedG Mela Clusters.
      void ComputeNoAssociatedGClusters();

      // Compute BestNLOApproximationVBF Mela Clusters.
      void ComputeBestNLOVBFApproxClusters();

      // Compute BestNLO{Z,W}HApproximation Mela Clusters.
      void ComputeBestNLOVHApproxClusters(char const V);
  };
}

#endif
